package main

import (
	"fmt"
	"os"
	"bufio"
	"sync"
	"net/http"
	"log"
	"github.com/PuerkitoBio/goquery"
	"github.com/joho/godotenv"
	"strings"
	"errors"
	"time"
	"strconv"
	"path/filepath"
	"encoding/json"
	"mymodule/pkg/utils"
	"mymodule/pkg/dictionary"
	"mymodule/pkg/proxy"
	"mymodule/pkg/redis"
)

type Output struct {
	Word string `json:"word"`
	Count int   `json:"count"`
}

type Record struct {
	Url string
	Wordcount map[string]int
}


func (r *Record) getEssay() (string, error) {
	// check if present in redis
	if redis.Client != nil {
		log.Printf("Sending a Redis key request to %s", r.Url)
		value, _ := redis.Client.Get(r.Url).Result()
		if value != "" {
			return value, nil
		}
	}
	
	log.Printf("Sending a HTTP request to %s", r.Url)
	resp, err := http.Get(r.Url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return "", err
	}
	if resp.StatusCode >= 200 && resp.StatusCode < 300 {
		essay, err := utils.PrepareEssay(doc)
		if err != nil {
			return "", err
		}
		// set to redis and fail silently if it fails
		if redis.Client != nil {
			redis.Client.Set(r.Url, essay, 7*24*time.Hour)
		}
		return essay, nil
	}

	// affected by rate limiting
	if resp.StatusCode == 999 {
		log.Printf("Making forward proxy call for url %s", r.Url)
		essay, err := proxy.MakeProxyCall(r.Url)
		if err != nil {
			log.Printf("Error: Couldn't fetch essay for %s from proxy server err %s", r.Url, err)
			return "", err
		}
		if redis.Client != nil {
			redis.Client.Set(r.Url, essay, 7*24*time.Hour)
		}
		return essay, nil
	}
	return "", errors.New(fmt.Sprintf("HTTP Error (%d) sent", resp.StatusCode))
}

func (r *Record) countWords(essay string) {
	wordCount := make(map[string]int)
	for _, word := range strings.Split(essay, " ") {
		word = utils.SantizeString(word)
		if len(word) >= 3 && utils.IsAlphabetOnly(word) {
			wordCount[word]++
		}
	}
	r.Wordcount = wordCount
}

func main() {
	// Load environment variables from .env file
	err := godotenv.Load()
	if err != nil {
		fmt.Println("Error loading .env file:", err)
		return
	}
	
	// set the logs to file
	loggerFilename := "./logs.txt"
	file, err := os.OpenFile(loggerFilename, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		log.Fatal("Error opening log file:", err)
	}
	log.SetOutput(file)

	// check if the proxy-scrapper is working is working or not
	proxy.WaitUntilProxyServiceAvailable()
	
	
	// Read list of Urls from the file
	filePath := os.Getenv("URL_LIST_PATH")
	file, err = os.Open(filePath)
	if err != nil {
		log.Fatalf("Error: opening file: %v", err)
	}
	log.Println("Read the url file successfully")
	defer file.Close()

	totalWordCount := make(map[string]int)
	urls := make(chan string)
	results := make(chan map[string]int)
	
	// start the execution
	var wg sync.WaitGroup
	numWorkers := 5
	wg.Add(numWorkers)

	// create multiple workers for making requests
	for i := 0; i < numWorkers; i++ {
		log.Printf("Creating a worker %d for making http request", i)
		go worker(urls, results, &wg, i)
	}
 
	// read th url from the file and push to the channel
	go func() {
		defer close(urls)
		log.Println("Writing url to the input channels")
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			time.Sleep(3 * time.Second)
			urls <- scanner.Text()
		}
	}()

	go func() {
		wg.Wait()
		log.Println("Completed processing urls by the worker")
		close(results)
	}()
  
	// Update the count of the word from all the essays 
	for result := range results {
		for key, value := range result { 
			totalWordCount[key] = totalWordCount[key] + value
		}
	}
  
	// filter words from dictionary
	filteredResult := dictionary.FilterFromDictionary(totalWordCount)
	sortedfilteredResult := utils.SortWord(filteredResult)
	// final result
	topWordsCount, _ := strconv.Atoi(os.Getenv("TOP_WORDS_COUNT"))
	outputs := make([]Output, 0, topWordsCount)
	endIndex := len(sortedfilteredResult)
	if len(sortedfilteredResult) > topWordsCount {
		endIndex = topWordsCount
	}
	
	for _, key := range sortedfilteredResult[0:endIndex] {
		outputs = append(outputs, Output{Word:key, Count:filteredResult[key]})
	}
	jsonData, _ := json.Marshal(outputs)
	
	// store this result to the file
	log.Println("Writing json to results file")
	resultDirectory := os.Getenv("RESULTS_FOLDER")
	utils.CreateDirectory(resultDirectory)
	utils.WriteResultToFile(filepath.Join(resultDirectory, utils.GenerateFileName()), jsonData)
}

func worker(urls <-chan string, results chan<- map[string]int, wg *sync.WaitGroup, workerNum int) {
	// create a map 
	defer wg.Done()
	for url := range urls {
		record := &Record{Url: url}
		essay, err := record.getEssay()
		if err != nil {
			log.Printf("ERROR: Countn't make HTTP request to %s, %s", record.Url, err)
			continue
		}
		record.countWords(essay)
		results <- record.Wordcount
	}
}