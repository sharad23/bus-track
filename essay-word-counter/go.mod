module mymodule

go 1.22.2

require (
	github.com/PuerkitoBio/goquery v1.9.1 // indirect
	github.com/andybalholm/cascadia v1.3.2 // indirect
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	golang.org/x/net v0.21.0 // indirect
)
