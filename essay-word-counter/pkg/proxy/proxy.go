package proxy

import (
	"log"
	"fmt"
	"net/http"
	"os"
	"github.com/PuerkitoBio/goquery"
	"mymodule/pkg/utils"
	"encoding/json"
	"errors"
	"net/url"
	"crypto/tls"
	"time"
)

type Request struct {
	TargetUrl string 
	ProxyUrl string   
	Doc *goquery.Document
}

func (r *Request) setProxyUrl() (error) {
	resp, err := http.Get(os.Getenv("PROXY_SCRAPER_URL"))
	if err != nil {
		return err
	}
	var data map[string]string;
	if resp.StatusCode >= 200 && resp.StatusCode < 300 {
		if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
			return err
		}
		fmt.Printf("Fetched proxy server ip which is %s for %s", data["ip"], r.TargetUrl)
		ip, _ := data["ip"]
		r.ProxyUrl = ip
		return nil
	}
	fmt.Printf("Error: Some Other status is sent from proxy scrapper for %s", r.TargetUrl)
	return errors.New(fmt.Sprintf("HTTP Error (%d) sent", resp.StatusCode))
}

func (r *Request) makeProxyRequest() (int, error) {
	// Parse the proxy URL
	proxy, err := url.Parse("http://" + r.ProxyUrl)
	if err != nil {
		return 500, err
	}

	// Create a custom HTTP transport with certificate verification disabled for the proxy request
	transport := &http.Transport{
		Proxy: http.ProxyURL(proxy),
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}

	// Create a custom HTTP client with the custom transport
	client := &http.Client{
		Transport: transport,
	}
	// Create a new GET request to the target URL
	req, err := http.NewRequest("GET", r.TargetUrl, nil)
	if err != nil {
		return 500, err
	}
	// Send the request via the proxy
	resp, err := client.Do(req)
	if err != nil {
		return 500, err
	}
	defer resp.Body.Close()
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return 500, err
	}
	r.Doc = doc
	return resp.StatusCode, nil
}

func (r *Request) getEssay() (string, error) {
	essay, err := utils.PrepareEssay(r.Doc)
	if err != nil {
		return "", err
	}
	return essay, nil
}

func MakeProxyCall(url string) (string, error){
	proxyRequest := &Request{TargetUrl: url}
	counter := 0
	var statusCode int
	// free proxy address are not that reliable so we try five times
	for {
		err := proxyRequest.setProxyUrl()
		if err != nil {
			break
		}
		
    // for all the other status we try again
		statusCode, _ = proxyRequest.makeProxyRequest()
		if statusCode >= 200 && statusCode < 300 {
			break
		}
		// threhold value for loop execeed
		if counter > 5 {
			break
		}
		counter++
	}
	if statusCode >=200 && statusCode < 300 {
		essay, err := proxyRequest.getEssay()
		return essay, err
	}

	return "", errors.New(fmt.Sprintf("Couldn't generate because (%d) sent", statusCode))
}

func WaitUntilProxyServiceAvailable() {
	counter := 0
	log.Println("Waiting for Proxy Scrapper to Start")
	for {
		counter++
		if counter > 20 {
			break
		}
		resp, err := http.Get(os.Getenv("PROXY_SCRAPER_URL"))
		if err != nil {
			time.Sleep(5 * time.Second)
			continue
		}
		if resp.StatusCode == 200 {
			log.Println("Proxy Scrapper Started")
			break;
		}
	}
}