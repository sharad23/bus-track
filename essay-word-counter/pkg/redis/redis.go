package redis

import (
	"os"
	"log"
	"github.com/go-redis/redis"
)

var Client *redis.Client 

func init() {
	
	Client = redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_HOST"), // Redis server address
		Password: "",               // No password set
		DB:       0,                // Use default DB
	})
	
	// Ping the Redis server to check if it's reachable
	pong, err := Client.Ping().Result()
	if err != nil {
		log.Println("Error connecting to Redis:", err)
		return
	}
	log.Println("Connected to Redis:", pong)
}