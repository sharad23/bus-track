package dictionary

import (
	"os"
	"log"
	"bufio"
	"net/http"
	"sync"
	"strings"
)

func getDictonary() map[string]bool {
	dictionaryWords := make(map[string]bool)
	response, err := http.Get(os.Getenv("DICTIONARY_URL"))
	if err != nil {
		log.Println("ERROR:", err)
		return dictionaryWords
	}
	defer response.Body.Close()
	
	// creating hash of dictionary which will be efficient for search
	scanner := bufio.NewScanner(response.Body)
	for scanner.Scan() {
		word := scanner.Text()
		dictionaryWords[strings.ToLower(word)] = true
	}
	log.Println("Fetched dictionary completely")
	return dictionaryWords
}

func worker(dictionary map[string]bool, workChannel <-chan string, resultChannel chan<- string, wg *sync.WaitGroup) {
	defer wg.Done()
	for word := range workChannel {
			if _, ok := dictionary[word]; !ok {
					resultChannel <- word
			}
	}
}

func FilterFromDictionary(wordcount map[string]int) map[string]int {
	var wg sync.WaitGroup
	dictionary := getDictonary()
	workChannel := make(chan string)
	resultChannel := make(chan string)
  
	// Create workers
	numWorkers := 5
	for i := 0; i < numWorkers; i++ {
			wg.Add(1)
			log.Printf("Worker %d for dictionary  filter started", i)
			go worker(dictionary, workChannel, resultChannel, &wg)
	}

	// Distribute work
	go func() {
		defer close(workChannel)
		for word := range wordcount {
			workChannel <- word
		}
	}()
	
	// Collect results
	go func() {
		wg.Wait()
		log.Println("Completed the dictionary filtering")
		close(resultChannel)
	}()
	
  // words to be removed
	wordsToBeRemoved := make([]string, 0, 10)
	for word := range resultChannel {
		wordsToBeRemoved = append(wordsToBeRemoved, word)
	}

	for _, word := range wordsToBeRemoved {
		delete(wordcount, word)
	}
	return wordcount
}