package utils

import (
	 "os"
   "sort"
   "unicode"
	 "strings"
	 "errors"
	 "log"
	 "github.com/PuerkitoBio/goquery"
	 "time"
)

func IsAlphabetOnly(str string) bool {
	for _, char := range str {
		if !unicode.IsLetter(char) {
			return false
		}
	}
	return true
}

func SortWord(wordCount map[string]int) []string {
	keys := make([]string, 0, len(wordCount))

	// Append keys to the slice
	for k := range wordCount {
		keys = append(keys, k)
	}
   // Sort the keys based on their corresponding values in descending order
	sort.Slice(keys, func(i, j int) bool {
		if wordCount[keys[i]] == wordCount[keys[j]] {
			return keys[i] < keys[j]
		}
		return wordCount[keys[i]] > wordCount[keys[j]]
	})

	return keys
}

func SantizeString(word string) string {
	return strings.Trim(strings.ToLower(word), ",.!?()[]")
}

func PrepareEssay(doc *goquery.Document) (string, error){
	header := doc.Find("#caas-lead-header-undefined").Text()
	body := doc.Find(".caas-body").Text()
	if len(header) == 0 && len(body) == 0 {
		return "", errors.New("Coundn't fetch essay")
	}
	essay := strings.Join([]string{header, body}, " ")
	return essay, nil
}

func CreateDirectory(dir string) {
	os.Mkdir(dir, 0755)
	return
}

func WriteResultToFile(filePath string, jsonData []byte)  {
	file, err := os.Create(filePath)
	if err != nil {
		log.Printf("Error: creating file: %v", err)
		return
	}
	defer file.Close()

	// Write the JSON data to the file
	_, err = file.Write(jsonData)
	if err != nil {
		log.Printf("Error: wriiting file: %v", err)
		return
	}
}

func GenerateFileName() string {
	currentTime := time.Now()

	// Format the current date and time as "YYYY-MM-DD HH:mm"
	formattedTime := currentTime.Format("2006-01-02 15:04")
	return formattedTime+".json"
}

