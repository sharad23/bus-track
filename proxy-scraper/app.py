from crypt import methods
from flask import Flask, jsonify
import subprocess

app = Flask(__name__)

# Global variable to store the last selected number
data = []
last_number = 0

@app.route('/', methods=['GET'])
def healthcheck():
  return jsonify({'status': 'success'})

# Define a route for the API endpoint
@app.route('/api/referesh_proxy', methods=['GET'])
def refresh():
  subprocess.check_output(['python3', 'proxyScraper.py', '-p', 'https'])
  subprocess.check_output(['python3', 'proxyChecker.py', '-t', '20', '-s', 'google.com', '-l', 'output.txt'])
  global data
  global last_number
  last_number = 0
  data = []
  with open('output.txt', 'r') as file:
      lines = file.readlines()
      # Iterate over each line and store it in the array
      for line in lines:
        # Remove newline character from each line and append to the array
        data.append(line.strip())
  return jsonify({'status': 'success'})

@app.route('/api/get_proxy', methods=['GET'])
def get_proxy():
  global last_number
  last_number = (last_number + 1) % len(data)
  return jsonify({'ip': data[last_number]})

if __name__ == '__main__':
    subprocess.check_output(['python3', 'proxyScraper.py', '-p', 'https'])
    subprocess.check_output(['python3', 'proxyChecker.py', '-t', '20', '-s', 'google.com', '-l', 'output.txt'])
    with open('output.txt', 'r') as file:
      lines = file.readlines()
      # Iterate over each line and store it in the array
      for line in lines:
        # Remove newline character from each line and append to the array
        data.append(line.strip())

    app.run(debug=True, host='0.0.0.0', port=5555)
