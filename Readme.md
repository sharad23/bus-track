# Essay word counter
This repository contains codebase for the essay word counter which call multiple sites from  endg-urls2.txt and fetches the essays and countes the top 10 words in go lang and a proxy scraper which scrapes the free proxy ip from multiple open source sites.

## Architecture
Basic Architecture diagram is here, https://www.edrawmax.com/online/share.html?code=6310cddafa1711eea33a0a54be41f961

We have three components: a Go application, a Python application, and a Redis database.

The Go application serves the functional requirements, performing various tasks as needed.

The Python application serves as a provider of free proxy IP addresses to the Go application.

Additionally, we utilize a Redis database to store essays. When the Go application makes a request to a URL and retrieves an essay, it stores the content in Redis. This way, the next time if the Go application needs 
call the same URL it will just key the value from redis, it can simply retrieve the content from Redis, avoiding the need to make another request to the URL.

### Basic Flow
The Go application operates by reading from a list of URLs and attempting to call each one. To achieve parallelism and concurrency, we implement a worker module. By default, we have 5 workers that concurrently fetch content from the URLs.

Before initiating calls to the URLs, the application checks Redis with the URL key. If the URL is not present in Redis, an actual call is made. If the application encounters a rate limiting status 999 while calling these URLs, it triggers a call to the proxy scraper to fetch a free proxy IP. With this IP, the application makes a forward proxy call to the URL.

Each goroutine responsible for fetching essays also performs filtering and sanitization tasks. Once all goroutines have completed their tasks, we generate a map of total words and their counts.

The final step involves validating these words against a dictionary. We've created a hash map of dictionary words to significantly improve the time complexity of the search. Utilizing goroutines, we search the total words against the dictionary to ensure their validity, and then present the sorted words.


## Getting Started

To start the entire application, follow these steps:

1. Make sure you have Docker installed on your machine.

2. Clone this repository:
    ```bash
    git clone <repository-url>
    ```

3. Navigate to the project directory:
    ```bash
    cd project-directory
    ```

4. Build the Docker images:
    ```bash
    docker-compose build
    ```


5. Start the application:
    ```bash
    docker-compose up
    ```
The result will be available inside the output directory of the current folder. Similary the logs will also be available on the logs.txt

Note: Since i am using a free proxy ip, its very slow and not that relaible. On real use case i would definetly use any service provider proxy for better results.